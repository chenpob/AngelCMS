﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AngelCMS.module.Models
{
    public class UserInfo
    {
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string TicketCode { get; set; }
        public int IsFinal { get; set; }
        public string Grades  { get; set; }
        public int MatchID { get; set; }

        public int AreaID { get; set; }
        
    }

    public class ReturnMsg {
        public string msg { get; set; }
        public int status { get; set; }
        public string result { get; set; }

    }




}