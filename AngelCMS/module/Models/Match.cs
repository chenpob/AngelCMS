﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AngelCMS.module.Models
{
    public class Match
    {
        public string MatchName { get; set; }
        public string MatchQuarter { get; set; }
        public string MatchArea { get; set; }
        public int IsFinalMatch { get; set; }
        public decimal MatchFee { get; set; }
        public DateTime? MatchTimeBegin { get; set; }
        public DateTime? MatchTimeEnd { get; set; }
        public DateTime? EntryTimeBegin { get; set; }
        public DateTime? EntryTimeEnd { get; set; }
        public DateTime? CreateTime { get; set; }
        public string QrCode { get; set; }
        public int iscurent { get; set; }

    }

    

    public class PageList {

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int Count { get; set; }
        public decimal ToalPage { get;set; }
        public int code { get; set; }
        public DataTable dt { get; set; }
    }


}