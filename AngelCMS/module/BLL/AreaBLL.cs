﻿using AngelCMS.module.DAO;
using AngelCMS.module.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AngelCMS.module.BLL
{
    public class AreaBLL
    {
        //AreaDAL areaDal = new AreaDAL();//dataAcess
        AreaDAO areaDal = new AreaDAO();//dataAcess

        public int AddModel(string m) {

            return areaDal.addModel(m);

        }

        public int IsExist(string AreaName,int ID) {

            return areaDal.IsExist(AreaName,ID);

        }

        public int Delete(int id)
        {
          return areaDal.Delete(id);
        }

        public int Edit(string m,int id) {
            return areaDal.Edit(m,id);
        }

        public DataTable GetModelById(string id) {

            return areaDal.GetModelById(Convert.ToInt32(id));
        }

        public PageList GetLists(string  keyword,int pagesize,int pageindex,string orderby,out int count) {
            PageList pl = new PageList();

           DataTable dt = areaDal.GetLists(keyword, pagesize, pageindex, orderby,out  count);
            pl.Count = count;
            pl.dt = dt;
            pl.PageSize = pagesize;
            pl.PageIndex = pageindex;
            pl.ToalPage = Math.Max((count + pagesize - 1) / pagesize, 1);
            return pl;
        }

        public DataTable GeSelectList()
        {
            return areaDal.GeSelectList();
        }
    }
}