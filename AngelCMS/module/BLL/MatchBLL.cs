﻿using AngelCMS.module.DAO;
using AngelCMS.module.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AngelCMS.module.BLL
{
    public class MatchBLL
    {
        MatchDAL mathDal = new MatchDAL();

        public int AddMatch(Match m) {

            return mathDal.addMatch(m);

        }

        public int Delete(int id)
        {
          return  mathDal.Delete(id);
        }

        public int Edit(Match m,int id) {
            return mathDal.Edit(m,id);
        }

        public DataTable GetMatchById(string id) {

            return mathDal.GetMatchById(Convert.ToInt32(id));
        }

        public PageList GetMatchs(string  keyword,int pagesize,int pageindex,string orderby,out int count) {
            PageList pl = new PageList();

           DataTable dt = mathDal.GetMatchs(keyword, pagesize, pageindex, orderby,out  count);
            pl.Count = count;
            pl.dt = dt;
            pl.PageSize = pagesize;
            pl.PageIndex = pageindex;
            pl.ToalPage = Math.Max((count + pagesize - 1) / pagesize, 1);
            return pl;
        }

        public int DeletePic(int id)
        {
            return mathDal.DeletePic(id);
        }
    }
}