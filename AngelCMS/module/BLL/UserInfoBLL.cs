﻿using AngelCMS.module.DAO;
using AngelCMS.module.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AngelCMS.module.BLL
{
    public class UserInfoBLL
    {
        UserInfoDAL UserInfoDal = new UserInfoDAL();

        public int AddUsersInfo(UserInfo m) {

            return UserInfoDal.AddUsersInfo(m);

        }

        public int Edit(UserInfo m,int id) {
            return UserInfoDal.Edit(m,id);
        }

        public int Delete(int id)
        {
          return UserInfoDal.Delete(id);
        }

        public DataTable GetUserInfoById(string id) {

            return UserInfoDal.GetUserInfoById(Convert.ToInt32(id));
        }

        public PageList GetList(string  keyword,int pagesize,int pageindex,string orderby,out int count) {
            PageList pl = new PageList();

           DataTable dt = UserInfoDal.GetUsersInfo(keyword, pagesize, pageindex, orderby,out  count);
            pl.Count = count;
            pl.dt = dt;
            pl.PageSize = pagesize;
            pl.PageIndex = pageindex;
            pl.ToalPage = Math.Max((count + pagesize - 1) / pagesize, 1);
            pl.code = 200;
            return pl;
        }

        public DataTable GetMatch(string matchArea, string phoneNumber, string userName, string matchName)
        {
            return UserInfoDal.GetMatch(matchArea,phoneNumber,userName,matchName);
        }


        public DataTable GetCurentMacthinfo() {
            return UserInfoDal.GetCurentMacthinfo();
        }

        public DataTable GeSelectList()
        {
            return UserInfoDal.GeSelectList();
        }

        public DataTable GeSelectList(string  MatchName)
        {
          return  UserInfoDal.GeSelectList(MatchName);
        }
    }
}