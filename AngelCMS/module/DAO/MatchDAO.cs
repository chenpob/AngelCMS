﻿using AngelCMS.module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace AngelCMS.module.DAO
{
    public class MatchDAO:BaseDao
    {
        AccessHelper ah = new AccessHelper();
        public int addMatch(Match m) {
          
            string sql = @"INSERT INTO MatchInfo(MatchName, MatchQuarter, MatchArea, IsFinalMatch, MatchFee, MatchTimeBegin, MatchTimeEnd,EntryTimeBegin,EntryTimeEnd,CreateTime,QrCode,iscurent)
                  VALUES
                   ('{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}','{7}','{8}','{9}','{10}','{11}')";
            string sqlformat =string.Format(sql,m.MatchName,m.MatchQuarter,m.MatchArea,m.IsFinalMatch,m.MatchFee,m.MatchTimeBegin, m.MatchTimeEnd,m.EntryTimeBegin,m.EntryTimeEnd,m.CreateTime,m.QrCode,m.iscurent);
            return  ah.excuteSqlInt(sqlformat);
        }

        public int Delete(int id) {

            string sql = "delete  from MatchInfo  where id=" + id;
             return ah.excuteSqlInt(sql);

        }

        public int Edit(Match m,int id)
        {
            string sql = @"Update MatchInfo set MatchName='{0}',MatchQuarter='{1}',MatchArea='{2}',
                                                IsFinalMatch='{3}',MatchFee={4},MatchTimeBegin='{5}',
                           MatchTimeEnd='{6}',EntryTimeBegin='{7}',EntryTimeEnd='{8}',QrCode='{9}',iscurent={11} where ID= {10}";
            string sqlformat = string.Format(sql, m.MatchName, m.MatchQuarter, m.MatchArea, m.IsFinalMatch, m.MatchFee, m.MatchTimeBegin, m.MatchTimeEnd, m.EntryTimeBegin, m.EntryTimeEnd, m.QrCode,id,m.iscurent);
            return ah.excuteSqlInt(sqlformat);
        }

        public int DeletePic(int id)
        {
            string sql = "Update MatchInfo set QrCode='' where ID= {0}";
            string sqlformat = string.Format(sql,id);
             return ah.excuteSqlInt(sqlformat);
        }

        public DataTable GetMatchById(int id) {

            string sql = "select*  from  MatchInfo where id=" + id;
            return ah.dataTable(sql);
        }
        public DataTable GetMatchs(string keyword, int pagesize, int pageindex, string orderby, out int count)
        {


            string fldName = @"*,Format(MatchTimeBegin,'yyyy-MM-dd') as MatchTimeBegin1,
                                 Format(MatchTimeEnd,'yyyy-MM-dd') as MatchTimeEnd1,
                                Format(EntryTimeBegin,'yyyy-MM-dd') as EntryTimeBegin1,
                                Format(EntryTimeEnd,'yyyy-MM-dd') as EntryTimeEnd1,
                                Format(CreateTime,'yyyy-MM-dd') as CreateTime1 ";
            string strWhere = "";
            if (!string.IsNullOrEmpty(keyword))
            {
                strWhere = string.Format("MatchName like '%{0}%' ", keyword);
            }

            return ah.FengYe("MatchInfo", fldName, "id","",strWhere, pagesize, pageindex,out  count);
        }
    }
}