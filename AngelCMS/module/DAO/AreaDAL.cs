﻿using AngelCMS.module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace AngelCMS.module.DAO
{
    public class AreaDAL
    {
        AccessHelper ah = new AccessHelper();


        public int addModel(string name) {
          
            string sql = @"INSERT INTO MatchArea (AreaName)
                  VALUES ('{0}')";
            string sqlformat =string.Format(sql,name);
            return  ah.excuteSqlInt(sqlformat);
        }

        

        public int IsExist(string AreaName,int ID) {
            DataTable dt = new DataTable();
            string strWhere = "";
            if (ID > 0) {
                strWhere = " AND ID <> "+ID;
            }
            string sql = "select*  from  MatchArea where AreaName='"+AreaName+"'"+strWhere;
             dt=ah.dataTable(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows.Count;
            }
            else {
                return 0;
            }
        }
        

        public int Delete(int id) {

            string sql = "delete  from MatchArea  where id=" + id;
             return ah.excuteSqlInt(sql);

        }

        public int Edit(string name, int id)
        {
            string sql = @"Update MatchArea set AreaName='{0}' ";
            string sqlformat = string.Format(sql, name,id);
            return ah.excuteSqlInt(sqlformat);
        }

        public DataTable GeSelectList()
        {
            string sql = "select*  from  MatchArea ";
            return ah.dataTable(sql);
        }

        public DataTable GetModelById(int id) {

            string sql = "select*  from  MatchArea where id=" + id;
            return ah.dataTable(sql);
        }
        public DataTable GetLists(string keyword, int pagesize, int pageindex, string orderby, out int count)
        {


            string fldName = @"*";
            string strWhere = "";
            if (!string.IsNullOrEmpty(keyword))
            {
                strWhere = string.Format("AreaName  like '%{0}%' ", keyword);
            }

            return ah.FengYe("MatchArea", fldName, "id","",strWhere, pagesize, pageindex,out  count);
        }
    }
}