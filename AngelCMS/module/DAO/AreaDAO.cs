﻿using AngelCMS.module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using AngelCMS.module.commons;

namespace AngelCMS.module.DAO
{
    public class AreaDAO:BaseDao
    {

        public int addModel(string name) {
          
            string sql = @"INSERT INTO MatchArea (AreaName)
                  VALUES ('{0}')";
            string sqlformat =string.Format(sql,name);
            return MySQLHelper.ExecuteNonQuery(connectionStr, CommandType.Text, sqlformat);
        }

        

        public int IsExist(string AreaName,int ID) {
            DataTable dt = new DataTable();
            string strWhere = "";
            if (ID > 0) {
                strWhere = " AND ID <> "+ID;
            }
            string sql = "select*  from  MatchArea where AreaName='"+AreaName+"'"+strWhere;
             dt= MySQLHelper.GetDataTable(connectionStr, CommandType.Text, sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows.Count;
            }
            else {
                return 0;
            }
        }
        

        public int Delete(int id) {

            string sql = "delete  from MatchArea  where id=" + id;
             return MySQLHelper.ExecuteNonQuery(connectionStr, CommandType.Text, sql);

        }

        public int Edit(string name, int id)
        {
            string sql = @"Update MatchArea set AreaName='{0}' ";
            string sqlformat = string.Format(sql, name,id);
            return MySQLHelper.ExecuteNonQuery(connectionStr, CommandType.Text, sql);
        }

        public DataTable GeSelectList()
        {
            string sql = "select*  from  MatchArea ";
            return MySQLHelper.GetDataTable(connectionStr, CommandType.Text, sql);
        }

        public DataTable GetModelById(int id) {

            string sql = "select*  from  MatchArea where id=" + id;
            return MySQLHelper.GetDataTable(connectionStr, CommandType.Text, sql);
        }
        public DataTable GetLists(string keyword, int pagesize, int pageindex, string orderby, out int count)
        {


           
            string strWhere = "";
            string sql = "select *  from MatchArea";
            if (!string.IsNullOrEmpty(keyword))
            {
                strWhere = string.Format("AreaName  like '%{0}%' ", keyword);
            }
            sql = sql + strWhere;
            return MySQLHelper.ExecuteSqlByMySQLClient1(sql, pageindex, pagesize,out  count, connectionStr);
        }
    }
}