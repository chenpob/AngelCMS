﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AngelCMS.module.commons
{
    public class ConfigHelper
    {
        public static string GetConnSettingsValue(string key)
        {
            ConfigurationManager.RefreshSection("connectionStrings");
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[key];
            return settings.ConnectionString;
        }
    }
}