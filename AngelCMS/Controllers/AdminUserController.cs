﻿using AngelCMS.module.BLL;
using AngelCMS.module.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngelCMS.Controllers
{
    public class AdminUserController : Controller
    {
        // GET: Admin

        UserInfoBLL userInfoBLL = new UserInfoBLL();
        public ActionResult Index()
        { 
            return View();
        }
     


        public string GeSelectList() {

            DataTable dt = new DataTable();
            dt = userInfoBLL.GeSelectList();
            return JsonConvert.SerializeObject(dt);
        }

        public string GetList() {
           DataTable dt = new DataTable();
           string keyword=Request["keyword"]==null?"": Request["keyword"];
           string  pageIndex= Request["pageIndex"]==null?"1":Request["pageIndex"];
           string pageSize= Request["pageSize"] == null ? "10" : Request["pageSize"];
           int count = 0;
            PageList pl = new PageList();
          pl= userInfoBLL.GetList(keyword,Convert.ToInt32(pageSize), Convert.ToInt32(pageIndex),"",out  count);

            return JsonConvert.SerializeObject(pl);
        }

        public int del(int id) {
           return userInfoBLL.Delete(id);
        }

        public int Add() {
            var form = Request.Form;            
            UserInfo m = new UserInfo();
            int ID = Convert.ToInt32(form["ID"]);
           
            m.UserName = form["UserName"];
            m.PhoneNumber = form["PhoneNumber"];
            m.TicketCode = form["TicketCode"];
            m.Grades = form["Grades"];
            m.MatchID =Convert.ToInt32(form["MatchID"]);
            m.AreaID= Convert.ToInt32(form["AreaID"]);
            m.IsFinal = form["FinalMatchN"] == null ? 0 : 1;//0表示没有进入决赛，1表示进入;

            if (ID == 0)
            {
                //m.IsFinalMatch = form["FinalMatchN"] == null ? 0 : 1;//0表示没有进入决赛，1表示进入
                return userInfoBLL.AddUsersInfo(m);
            }
            else {
                //m.IsFinalMatch = form["FinalMatchN"] == null ? 0 : 1;
                return userInfoBLL.Edit(m,ID);
            }          
        }
    }
}