﻿using AngelCMS.module.BLL;
using AngelCMS.module.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngelCMS.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin

        MatchBLL matchBLL = new MatchBLL();
        public ActionResult Index()
        { 
            return View();
        }
        public ActionResult EditMatch(string id) {
            List<Match> m = new List<Match>(); ;
            if (!string.IsNullOrEmpty(id)&&id!="0")
            {
                ViewBag.ID = id;
                DataTable dt = matchBLL.GetMatchById(id);
                m = ModelConvertHelper<Match>.ConvertToModel(dt);
                ViewBag.Match = m.First();
            }
            else {
                ViewBag.Match = new Match();
                ViewBag.ID = "0";
            }
          
            return View();
        }

        /// <summary>
        /// 删除图片
        /// </summary>
        /// <returns></returns>
        public int DeletePic() {
            string id = Request["key"] == null ? "" : Request["key"];
          return  matchBLL.DeletePic(Convert.ToInt32(id));

        }

        public int del(int id)
        {
            return matchBLL.Delete(id);
        }

        public string GetMatch() {
           DataTable dt = new DataTable();
           string keyword=Request["keyword"]==null?"": Request["keyword"];
           string  pageIndex= Request["pageIndex"]==null?"":Request["pageIndex"];
           string pageSize= Request["pageSize"] == null ? "" : Request["pageSize"];
           int count = 0;
            PageList pl = new PageList();
          pl=  matchBLL.GetMatchs(keyword,Convert.ToInt32(pageSize), Convert.ToInt32(pageIndex),"",out  count);

            return JsonConvert.SerializeObject(pl);
        }
        public int AddMatch() {
            var form = Request.Form;            
            Match m = new Match();
            int ID = Convert.ToInt32(form["ID"]);
            string qrcodeUrl = form["qrcodeUrl"];
            var iscurent = form["iscurent"];
            if (iscurent == "on")
            {
                m.iscurent = 1;
            }
            else {
                m.iscurent = 0;
            }
            m.CreateTime = DateTime.Now;
            m.MatchArea = form["MatchArea"];
            m.MatchFee =Convert.ToDecimal(form["MatchFee"]);
            m.MatchName = form["MatchName"];
            m.MatchQuarter = form["MatchQuarter"];
            m.MatchTimeBegin = form["MatchTimeBegin"]==""? DateTime.Now: Convert.ToDateTime(form["MatchTimeBegin"]);
            m.MatchTimeEnd = form["MatchTimeEnd"] == "" ? DateTime.Now : Convert.ToDateTime(form["MatchTimeEnd"]);           
            m.IsFinalMatch = 0;
            m.EntryTimeBegin = form["EntryTimeBegin"] == "" ? DateTime.Now : Convert.ToDateTime(form["EntryTimeBegin"]);
            m.EntryTimeEnd = form["EntryTimeEnd"] == "" ? DateTime.Now : Convert.ToDateTime(form["EntryTimeEnd"]);
            if (Request.Files.Count > 0)
            {
                string qrcode = DateTime.Now.ToString("yyyyMMddHHmmss");
                var oFile = Request.Files[0];
          
                string extend = oFile.FileName.Substring(oFile.FileName.LastIndexOf("."));
                string filename = qrcode + extend;
                m.QrCode = filename;
                if (oFile.FileName.EndsWith(".jpg") || oFile.FileName.EndsWith("png"))
                {
                    string updir = Server.MapPath("/QrCode/");
                    if (!Directory.Exists(updir))
                    { Directory.CreateDirectory(updir); }
                    string oldFullName = Path.Combine(updir, filename);
                    oFile.SaveAs(oldFullName);
                  
                }

              
            }
            if (ID == 0)
            {
                m.IsFinalMatch = form["FinalMatchN"] == null ? 0 : 1;//0表示没有进入决赛，1表示进入
                return matchBLL.AddMatch(m);
            }
            else {
                if (qrcodeUrl != null&& Request.Files.Count<=0) {
                    m.QrCode = qrcodeUrl;
                }
                m.IsFinalMatch = form["FinalMatchN"] == null ? 0 : 1;
                return matchBLL.Edit(m,ID);
            }          
        }
    }
}