﻿using AngelCMS.module.BLL;
using AngelCMS.module.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngelCMS.Controllers
{
    public class AreaController : Controller
    {
        // GET: Admin

        AreaBLL BLL = new AreaBLL();
        public ActionResult Index()
        { 
            return View();
        }
        public ActionResult Edit(string id) {
            List<string> m = new List<string>(); ;
            if (!string.IsNullOrEmpty(id)&&id!="0")
            {
                ViewBag.ID = id;
                DataTable dt = BLL.GetModelById(id);
                
                ViewBag.Area =dt;
            }
            else {
                ViewBag.Area = "";
                ViewBag.ID = "0";
            }
          
            return View();
        }

        public string GeSelectList()
        {

            DataTable dt = new DataTable();
            dt = BLL.GeSelectList();
            return JsonConvert.SerializeObject(dt);
        }

        public int del(int id)
        {
            return BLL.Delete(id);
        }

        public string GetLists() {
           DataTable dt = new DataTable();
           string keyword=Request["keyword"]==null?"": Request["keyword"];
           string  pageIndex= Request["pageIndex"]==null?"":Request["pageIndex"];
           string pageSize= Request["pageSize"] == null ? "" : Request["pageSize"];
           int count = 0;
            PageList pl = new PageList();
          pl=  BLL.GetLists(keyword,Convert.ToInt32(pageSize), Convert.ToInt32(pageIndex),"",out  count);

            return JsonConvert.SerializeObject(pl);
        }
        public int AddModel() {
            var form = Request.Form;            
            int ID = Convert.ToInt32(form["ID"]);
             string AreaName = form["AreaName"];
            if (BLL.IsExist(AreaName,ID) > 0)
            {
                return -1;
            }
            if (ID == 0)
            {               
                    return BLL.AddModel(AreaName);              
            }
            else {              
                return BLL.Edit(AreaName, ID);
            }          
        }
    }
}