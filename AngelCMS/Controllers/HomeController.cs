﻿using AngelCMS.module.BLL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngelCMS.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home

        UserInfoBLL userInfoBLL = new UserInfoBLL();
        public ActionResult Index()
        {
            DataTable dt = new DataTable();
            dt = userInfoBLL.GetCurentMacthinfo();
            ViewBag.Macthdt = dt;
            return View();
        }

        
        public string GeSelectList(string MatchName)
        {

            DataTable dt = new DataTable();
            dt = userInfoBLL.GeSelectList(MatchName);
            return JsonConvert.SerializeObject(dt);
        }


        public ActionResult ResultIndex() {
            DataTable dt = new DataTable();
            var Form = Request.Form;
            if (Form.Count > 0){ 
            string MatchArea = Form["MatchArea"].Trim();
            string PhoneNumber = Form["PhoneNumber"].Trim();
            string UserName = Form["UserName"].Trim();
            string MatchName = Form["MatchName"].Trim();              
                dt = userInfoBLL.GetMatch(MatchArea, PhoneNumber, UserName, MatchName);
            }
            ViewBag.Match = dt;
            return View();

    }
    }
}